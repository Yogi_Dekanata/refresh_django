"""readit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles import views
from django.views.generic import ListView
from filebrowser.sites import site

from books.models import Book
from books.views import AuthorList, BooksDetails, AuthorDetails, ReviewBooks, ReviewBook, AuthorCreate

urlpatterns = [


    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    #usermanagement'
    url(r'^accounts/', include('allauth.urls')),

    # url(r'^login/$', login, {'template_name': 'login.html'}, name='login'),
    # url(r'^logout/$', logout, {'next_page': 'books'}, name='logout'),
    ###############

    url(r'^admin/', admin.site.urls),
    url(r'^$', ListView.as_view(
        model=Book,
        paginate_by='10',
        queryset=Book.objects.exclude(date_review__isnull=True).prefetch_related("authors"),
        context_object_name="books",
        template_name='list-book.html'),
        name="books",
        ),
    url(r'^authors/list/$', AuthorList.as_view(), name="authors"),
    url(r'^authors/add/$', login_required(AuthorCreate.as_view()), name="add-author"),
    url(r'^authors/(?P<slug>[\w-]+)/$', AuthorDetails.as_view(), name="author-detail"),

    # url(r'^books/(?P<pk>[\w-]+)/$', BooksDetails.as_view(), name="book-detail"),
    url(r'^books/(?P<slug>[^\.]+)/$', BooksDetails.as_view(), name="book-detail"),

    # url(r'^authors/(?P<pk>[\w-]+)/$', AuthorDetails.as_view(), name="author-detail"),
    url(r'^review/(?P<slug>[^\.]+)/$', login_required(ReviewBook.as_view()), name="review-book"),
    url(r'^review/',login_required(ReviewBooks.as_view()), name="review"),

    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    url(r'^static/(?P<path>.*)$', views.serve),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
    }),

]

