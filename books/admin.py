from django.contrib import admin
from .models import Book, Author
from .forms import BookForm
# from .forms import VerificationAdminForm


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    # form = BookForm
    list_per_page = 30
    fieldsets = [
        ("Books details", {"fields": ["title", "authors"]}),
        ("Review", {"fields": ["is_favorite", "book_review", "date_review", "review_by",]})
    ]
    readonly_fields = ("date_review","book_review")

    list_display = ("title", "id", "slug" , "books_author", "is_favorite", "book_review", "created", "modified",
                    "date_review","review_by",)

    list_editable = ("is_favorite",)
    list_filter = ("is_favorite",)
    list_display_links = ("date_review", "title")
    search_fields = ("authors__name",)

    def books_author(self, obj):
        return obj.list_authors()

    def author_id(self, obj):
        return obj.list_authors_id()

    def book_review(self, obj):
        return " %s " % obj.content_review()
    book_review.allow_tags = True


    books_author.short_description = "Author"
    author_id.short_description = "Ids"


#
# class MemberAdmin(admin.ModelAdmin):
#     list_display = ("title",)
#     inlines = [
#         Book,
#     ]
admin.site.register(Author)
# admin.site.register(Book, BookAdmin)
