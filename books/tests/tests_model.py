from django.test import TestCase
from books.models import Book
from books.factories import AuthorFactory, BookFactory


# Create your tests here.
class BookTest(TestCase):
    def setUp(self):
        self.author1 = AuthorFactory(name="Author1")
        self.author2 = AuthorFactory(name="Author2")
        self.book = Book(title="My book 1")
        self.book.save()
        self.book.authors.add(self.author1.pk, self.author2.pk)

    def tearDown(self):
        self.author1.delete()
        self.author2.delete()
        self.book.delete()

    def test_list_authors(self):
        self.assertEqual("Author2,  Author1", self.book.list_authors())

    def test_string_book(self):
        self.assertEqual("My book 1 by Author2,  Author1", self.book.__str__())
