from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import View, DetailView, ListView
from django.views.generic.edit import CreateView

from .forms import ReviewForm, BookForm
from .models import Book, Author


# Create your views here.
# def list_book(request):
#     contex = {"books": Book.objects.exclude(date_review__isnull=True).prefetch_related("authors")}
#     return render(request, 'list-book.html', contex)

def login_home(request):
    context = {'request': request, 'user': request.user}
    return render(request, 'registration/login.html', context)


# class ListBook(View):
#     def get(self, request):
#         contex = {"books": Book.objects.exclude(date_review__isnull=True).prefetch_related("authors")}
#         # contex = {"books":Book.objects.all()}
#         return render(request, 'list-book.html', contex)


class AuthorList(View):
    def get(self, request):
        authors = Author.objects.annotate(
            published_books=Count("books")
        ).filter(published_books__gt=0)
        # print authors
        # authors = Author.objects.all()
        contex = {"authors": authors}
        return render(request, "authors.html", contex)


class BooksDetails(DetailView):
    model = Book
    template_name = "books.html"


class BookDetailsSlug(View):
    model = Book

    def get(self, request, slug):
        contex = {"books": Book.objects.exclude(date_review__isnull=True).prefetch_related("authors"),}
        return render(request, 'books.html', contex)


class AuthorDetails(DetailView):
    model = Author
    template_name = "author.html"


class AuthorCreate(CreateView):
    model = Author
    fields = ['name']
    template_name = 'add-author.html'

    def get_success_url(self):
        return (reverse('review'))


class ReviewBooks(View):
    form = BookForm
    template_name = 'list-to-review.html'

    def get(self, request):
        books = Book.objects.exclude(date_review__isnull=True).prefetch_related("authors")
        contex = {"books": books, "form": self.form }
        return render(request, self.template_name, contex)

    def post(self, request):
        books = Book.objects.exclude(date_review__isnull=True).prefetch_related("authors")
        form = self.form(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Book review alredy Create.')
            return redirect(reverse('review'))
        contex = {"books": books, "form": form}
        return render(request, self.template_name, contex)


class ReviewBook(View):
    model = Book
    form = ReviewForm
    template_name = "review-book.html"

    def get(self, request, slug):
        book = get_object_or_404(Book, slug=slug)
        form = ReviewForm(initial={'review': book.review})
        contex = {"book": book, "form": form}
        return render(request, self.template_name, contex)

    def post(self, request, slug):
        book = get_object_or_404(Book, slug=slug)
        form = self.form(request.POST)
        if form.is_valid():
            book.is_favorite = form.cleaned_data['is_favourite']
            book.review = form.cleaned_data['review']
            book.review_by = request.user
            book.save()
            # return redirect(reverse('review-book', kwargs={'slug': slug}))
            messages.success(request, 'Book review alredy updated.')
            return redirect(reverse('review'))

        contex = {"book": book, "form": form}
        return render(request, self.template_name, contex)
