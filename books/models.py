from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField

from django_extensions.db.models import (TitleSlugDescriptionModel, TimeStampedModel, AutoSlugField)
import datetime


# Create your models here.
class Book(TimeStampedModel):
    title = models.CharField(max_length=150)
    authors = models.ManyToManyField('Author', related_name='books')
    date_review = models.DateTimeField(null=True, blank=True)
    review_by = models.ForeignKey(User, blank=True, null=True, related_name='reviews')
    slug = models.CharField(max_length=150, blank=True, null=True)
    review = RichTextField()
    is_favorite = models.BooleanField(default=False, verbose_name="Is Favorite ?")

    def list_authors(self):
        return ",  ".join([i.name for i in self.authors.all()]).title()

    def list_authors_id(self):
        return " ".join([str(i.slug) for i in self.authors.all()])

    def content_review(self):
        return self.review

    def get_absolute_url(self):
        return reverse('book-detail', kwargs={'slug': self.slug})

    def get_review_absolute_url(self):
        return reverse('review-book', kwargs={'slug': self.slug})

    def __unicode__(self):
        return "%s by %s" % (self.title, self.list_authors())

    def save(self, *args, **kwargs):
        # super(Book, self).save(*args, **kwargs)
        self.date_review = now()
        self.slug = '%s/%s' % (
            self.id, slugify(self.title)
        )
        super(Book, self).save(*args, **kwargs)


class Author(TimeStampedModel):
    name = models.CharField(help_text="add your name", unique=True, max_length=50)
    # slug = AutoSlugField(populate_from='name', unique=True, db_index=True)
    slug = AutoSlugField(populate_from='name', max_length=120, primary_key=True)

    def get_absolute_url(self):
        return reverse('author-detail', kwargs={'slug': self.slug})

    def __unicode__(self):
        return "%s" % (self.name)
